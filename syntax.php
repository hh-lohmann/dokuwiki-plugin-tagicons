<?php 
/** 
 * tagicons
 * DOC TO FOLLOW
 * ! BASIC REPLACE PATTERN BORROWED FROM Doku_Parser_Mode_smiley IN DokuWiki's parser.php
 */ 
 
if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');
 
/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_tagicons extends DokuWiki_Syntax_Plugin {
 
    /**
     * Get syntax plugin type.
     *
     * @return string The plugin type.
     */
    function getType() { return 'substition'; }

    /**
     * Get sort order of syntax plugin.
     *
     * @return int The sort order.
     */
    function getSort() { return 230; }

    /**
     * Get paragraph type.
     *
     * @return string The paragraph type.
     */
    function getPType() { return 'normal'; }

    /**
     * Connect patterns/modes
     *
     * @param $mode mixed The current mode
     */
    function connectTo($mode) {  
  		$this->Lexer->addSpecialPattern( '(?<=\W|^)FAILED(?=\W|$)' ,$mode,'plugin_tagicons');
  		$this->Lexer->addSpecialPattern( '(?<=\W|^)FIXED(?=\W|$)' ,$mode,'plugin_tagicons');
  		$this->Lexer->addSpecialPattern( '(?<=\W|^)INFO(?=\W|$)' ,$mode,'plugin_tagicons');
  		$this->Lexer->addSpecialPattern( '(?<=\W|^)OBSOLETE(?=\W|$)' ,$mode,'plugin_tagicons');
  		$this->Lexer->addSpecialPattern( '(?<=\W|^)RUNNING(?=\W|$)' ,$mode,'plugin_tagicons');
    }

 
    // Handle the match
    function handle($match, $state, $pos, &$handler) {
      return array ( $match ) ;
	  }
 
    // Create output
    function render($mode, &$renderer, $data) {
      if($mode == 'xhtml'){
        $renderer->doc .= '<img src="' . DOKU_URL . 'lib/plugins/tagicons/images/' . strtolower ( $data[0] ) . '.png" class="icon" alt="' . $data[0]. '">' ;
      }
      return false;
    }
}
